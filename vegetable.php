<?php
require_once "Product.php";

class Vegetable extends Product
{
    
    private $productorName;
    private $expiresAt;


    public function __construct( $c_id, $c_name, $productorName, $c_price, $c_expiresAt)
    {
        parent::__construct($c_id, $c_name, $c_price);
        $this->productorName = $productorName;
        $this->expiresAt = $c_expiresAt;
    }

    public function getproductorName(){
        return $this->productorName;
    }
 
    public function show(){
        echo '<p>' . $this->getName() . '</p>';
    }
}
   

// Créer une classe Vegetable héritant de Product et 
// ayant les attributs et méthodes suivants :
// * productorName
// * expiresAt
// * isFresh()

// Implémenter la méthode fresh qui retourne un booléen, 
// vrai si le produit peut être consommé
//  (avant sa date d'expiration), faux sinon.
