<?php

// Créer une classe Product ayant les attributs et méthodes suivants :
// * id
// * name
// * price
// * et les accesseurs nécessaires

class Product {
    private $id;
    private $name;
    private $price;
    public function __construct($c_id, $c_name, $c_price){
        $this->id = $c_id;
        $this->name = $c_name;
        $this->price = $c_price;
    }
    public function getId(){
        return $this->id;
    }
    public function getName(){
        return $this->name;
    }
    public function getPrice(){
        return $this->price;
    }
}


