<?php
require_once 'vegetable.php';

$peremption = new DateTime();
$peremption->add(new DateInterval('P4D'));

$leek = new Vegetable(1, "poireau", "productor", 3, $peremption);

$leek->show();
