<?php 
//creation des instances 
class User {
    private $id;
    private $email;
    private $createAt;
    //function constructor : evite de faire 1 fonction par instance 
    public function __construct($c_id, $c_email, $c_createAt){
        $this->id = $c_id;
        $this->email = $c_email;
        $c_createAt = date("Y-m-d H:i:s");
        $this->createAt = $c_createAt;
    }
    //functions getter (récupération des valeurs) : 1 getter par instance 
    public function getId(){
        return $this->id; 
    }
    public function getEmail(){
        return $this->email;
    }
    public function getCreateAt(){
        return $this->createAt;
    }
}


